const express = require('express')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const http = require('http')
const fs = require('fs')
const path = require ('path')
const axios = require('axios')
const Winston = require('./winston')

class Init{
    constructor() {
        if (process.env.NODE_ENV !== 'production') require('dotenv').config()

        const customWinston = new Winston()

        app.use(cors())

        app.use(customWinston.requestLoggerMiddleware)

        app.use(bodyParser.json());

        app.use(bodyParser.json({
            limit: '1mb'
        }))

        app.get('/teste', async (req, res) => {

            try{
                throw Error("deu ruim")
            }catch (e) {
                customWinston.errorLogger(req, res, e)
                res.end()
            }
        })

        app.post('/send', async (req, res) => {
            const {token,hello} = req.body
            const secret_key = process.env.RECAPTCHA_KEY;
            const url = `https://www.google.com/recaptcha/api/siteverify?secret=${secret_key}&response=${token}`;

            const result = await axios.post(url,{})

            console.log(result)

            res.json({result:String(hello)})

        })
    }

    createServer(){
        const server = http.createServer(app).listen(
            Number(process.env.PORT),
            String(process.env.HOST_NAME), () => console.log("Aplicação rodando na porta ",process.env.PORT))

        const io = require('socket.io')(server)

        io.on('connection', socket => {
            console.log(`Socket conectado. ID: ${socket.id}`);
            console.log(socket)
        })
    }
}

module.exports = new Init().createServer()