const winston = require('winston')

class Winston{
    constructor() {
        this.remoteLog = new winston.transports.Http({
            host: "localhost",
            port: 3008,
            path: "/errors"
        })

        this.consoleLog = new winston.transports.Console()
    }

    requestLoggerMiddleware = (req,res, next) => {
        const requestLogger = winston.createLogger({
            format: this.getRequestLogFormatter(req.ip),
            transports: [this.consoleLog]
        })

        requestLogger.info({req, res})
        next()
    }

    errorLogger = (req, res, error) => {
        const errLogger = winston.createLogger({
            format: this.getRequestLogFormatter(req.ip, error),
            level: 'error',
            transports: [this.remoteLog, this.consoleLog]
        })
        errLogger.error({req, res})
    }

    getRequestLogFormatter = (ip, error = null) => {
        const {combine, timestamp, printf} = winston.format;

        return combine(
            timestamp(),
            printf(info => {
                const {req} = info.message
                return `${info.timestamp} ${info.level}: ip:${ip} ${req.hostname}${req.port || ''}${req.originalUrl} error:${error != null? String(error) : ''}`;
            })
        );
    }
}

module.exports = Winston